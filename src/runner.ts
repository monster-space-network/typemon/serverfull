import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Scope } from '@typemon/scope';
import { Container, Constructor, Identifier } from '@typemon/dependency-injection';
import Express from 'express';
import Boom from '@hapi/boom';
//
import { MetadataKey } from './metadata-key';
import { Controller } from './controller';
import { Handler } from './handler';
import { Middleware, Next } from './middleware';
import {
    HttpRequest,
    HttpResponse,
    HttpHeaders,
    PathParameters, QueryStringParameters, Body
} from './http';
import { ParameterResolver } from './parameter-resolver';
import { Context } from './scoped-context';
//
//
//
export class Runner {
    public static async run(context: Runner.Context): Promise<void> {
        const runner: Runner = new Runner(context);

        await runner.run();
    }

    private readonly dependenciesContainer: Container;
    private readonly parametersContainer: Container;
    private readonly parameterResolver: ParameterResolver;

    private readonly target: Constructor;
    private readonly propertyKey: string;

    private readonly controllerMetadata: Metadata;
    private readonly controllerOptions: Controller.Options;

    private readonly handlerMetadata: Metadata;
    private readonly handlerOptions: Handler.Options;

    private readonly middlewares: ReadonlyArray<Middleware.Constructor>;

    private readonly expressRequest: Express.Request;
    private readonly expressResponse: Express.Response;

    private readonly request: HttpRequest;
    private readonly response: HttpResponse;

    private constructor(context: Runner.Context) {
        this.dependenciesContainer = context.container;
        this.parametersContainer = new Container();
        this.parameterResolver = new ParameterResolver(this.dependenciesContainer, this.parametersContainer);

        this.target = context.target;
        this.propertyKey = context.propertyKey;

        this.controllerMetadata = Metadata.of({
            target: this.target
        });
        this.controllerOptions = this.controllerMetadata.getOwn(MetadataKey.OPTIONS);

        this.handlerMetadata = Metadata.of({
            target: this.target.prototype,
            propertyKey: this.propertyKey
        });
        this.handlerOptions = this.handlerMetadata.getOwn(MetadataKey.OPTIONS);

        this.middlewares = context.middlewares
            .concat(this.controllerOptions.middlewares ?? [])
            .concat(this.handlerOptions.middlewares ?? []);

        this.expressRequest = context.request;
        this.expressResponse = context.response;

        this.request = new HttpRequest(this.expressRequest);
        this.response = new HttpResponse();

        this.parametersContainer.bind({
            identifier: HttpRequest,
            useValue: this.request
        });
        this.parametersContainer.bind({
            identifier: HttpResponse,
            useValue: this.response
        });
        this.parametersContainer.bind({
            identifier: HttpHeaders,
            useValue: this.request.headers
        });
        this.parametersContainer.bind({
            identifier: PathParameters.TOKEN,
            useValue: this.request.pathParameters
        });
        this.parametersContainer.bind({
            identifier: QueryStringParameters.TOKEN,
            useValue: this.request.queryStringParameters
        });
        this.parametersContainer.bind({
            identifier: Body.TOKEN,
            useValue: this.request.body
        });
        this.parametersContainer.bind({
            identifier: Context,
            useValue: new Context()
        });
    }

    private async executeMiddleware(target: Middleware.Constructor): Promise<boolean> {
        let nextable: boolean = false;
        const handlerMetadata: Metadata = Metadata.of({
            target,
            propertyKey: 'handler'
        });
        const next: Next = (): void => {
            nextable = true;
        };
        const parameters: ReadonlyArray<unknown> = await this.parameterResolver.resolveParameters(handlerMetadata, (identifier: Identifier, metadata: Metadata): unknown => {
            switch (identifier) {
                case Next.TOKEN: return next;

                default: return this.parameterResolver.resolveParameter(identifier, metadata);
            }
        });
        const instance: Middleware = await this.dependenciesContainer.resolve(target);

        await instance.handler(...parameters);

        return nextable;
    }
    private async executeMiddlewares(): Promise<boolean> {
        for (const middleware of this.middlewares) {
            const nextable: boolean = await this.executeMiddleware(middleware);

            if (Check.isFalse(nextable)) {
                return false;
            }
        }

        return true;
    }

    private async executeHandler(): Promise<unknown> {
        const parameters: ReadonlyArray<unknown> = await this.parameterResolver.resolveParameters(this.handlerMetadata);
        const instance: any = await this.dependenciesContainer.resolve(this.target);
        const output: unknown = await instance[this.propertyKey](...parameters);

        return output;
    }

    private async handleResult(output?: unknown): Promise<void> {
        this.expressResponse.status(this.response.statusCode);
        this.expressResponse.send(output ?? this.response.body);
    }

    private async execute(): Promise<void> {
        try {
            const nextable: boolean = await this.executeMiddlewares();

            if (Check.isFalse(nextable)) {
                return this.handleResult();
            }

            const output: unknown = await this.executeHandler();

            this.handleResult(output);
        }
        catch (error) {
            const boom: Boom.Boom = Boom.isBoom(error)
                ? error
                : Boom.internal();

            this.response.statusCode = boom.output.statusCode;
            this.handleResult(boom.output.payload);
        }
    }

    private run(): Promise<void> {
        return Scope.run((): Promise<void> => this.execute());
    }
}
export namespace Runner {
    export interface Context {
        readonly container: Container;
        readonly middlewares: ReadonlyArray<Middleware.Constructor>;

        readonly target: Constructor;
        readonly propertyKey: string;

        readonly request: Express.Request;
        readonly response: Express.Response;
    }
}
