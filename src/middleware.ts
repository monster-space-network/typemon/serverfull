import { Reflection, Metadata } from '@typemon/reflection';
import { MetadataKey, Injectable, Inject, InjectionToken } from '@typemon/dependency-injection';
import { Pipeable } from '@typemon/pipeline';
//
import { HandlerParameterDecorator } from './handler-parameter-decorator';
//
//
//
export interface Middleware {
    handler(...parameters: ReadonlyArray<any>): void | Promise<void>;
}
export function Middleware(): ClassDecorator {
    return Metadata.Decorator.create((metadata: Metadata): void => {
        if (metadata.hasOwn(MetadataKey.INJECTABLE)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        Reflection.decorate([Injectable()], metadata.owner);
    });
}
export namespace Middleware {
    export type Constructor = new (...parameters: ReadonlyArray<any>) => Middleware;
}

export type Next = () => void;
export function Next(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata) => Reflection.decorate([
        Inject(Next.TOKEN),
        Pipeable(false)
    ], metadata.owner));
}
export namespace Next {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverfull.middleware.next');
}
