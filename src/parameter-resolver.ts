import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Container, Identifier, MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
import { Pipeline } from '@typemon/pipeline';
//
import { MetadataKey } from './metadata-key';
import { ReadonlyHttpParameters, HttpHeaders, PathParameters, QueryStringParameters } from './http';
//
//
//
//
export class ParameterResolver {
    public constructor(
        private readonly dependenciesContainer: Container,
        private readonly parametersContainer: Container
    ) { }

    private async resolveHttpParameters(identifier: Identifier, metadata: Metadata, metadataKey: symbol): Promise<unknown> {
        const parameters: ReadonlyHttpParameters = await this.parametersContainer.get(identifier);
        const key: null | string = metadata.hasOwn(metadataKey)
            ? metadata.getOwn(metadataKey)
            : null;

        if (Check.isNull(key)) {
            return parameters;
        }

        if (parameters.has(key)) {
            return parameters.get(key);
        }

        return null;
    }

    public resolveParameter(identifier: Identifier, metadata: Metadata): Promise<unknown> {
        switch (identifier) {
            case HttpHeaders: return this.resolveHttpParameters(identifier, metadata, MetadataKey.NAME);

            case PathParameters.TOKEN:
            case QueryStringParameters.TOKEN: return this.resolveHttpParameters(identifier, metadata, MetadataKey.KEY);

            default: return this.parametersContainer.get(identifier);
        }
    }

    public async resolveParameters(metadata: Metadata, customResolver?: ParameterResolver.Resolver): Promise<ReadonlyArray<unknown>> {
        const resolver: ParameterResolver.Resolver = customResolver ?? this.resolveParameter.bind(this);
        const parametersMetadata: ReadonlyArray<Metadata> = Array.from({ length: metadata.getOwn(DIMetadataKey.PARAMETERS_LENGTH) }, (_: never, index: number): Metadata => metadata.of(index));
        const parameters: Array<unknown> = [];

        for (const parameterMetadata of parametersMetadata) {
            const identifier: Identifier = parameterMetadata.getOwn(DIMetadataKey.IDENTIFIER);
            const parameter: unknown = await resolver(identifier, parameterMetadata);
            const pipedParameter: unknown = await Pipeline.resolve(parameter, parameterMetadata, this.dependenciesContainer);

            parameters.push(pipedParameter);
        }

        return parameters;
    }
}
export namespace ParameterResolver {
    export type Resolver = (identifier: Identifier, metadata: Metadata) => unknown;
}
