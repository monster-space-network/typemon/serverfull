import { Check } from '@typemon/check';
import { Constructor, Provider, Container } from '@typemon/dependency-injection';
import createExpressApplication, { Application as ExpressApplication, Handler as ExpressHandler } from 'express';
import { Server as HttpServer } from 'http';
//
import { Middleware } from './middleware';
import { Resolver } from './resolver';
//
//
//
export class Application {
    private readonly container: Container;

    private readonly expressApplication: ExpressApplication;
    private readonly httpServer: HttpServer;
    private initialized: boolean;

    public constructor(
        private readonly config: Application.Configuration
    ) {
        this.container = new Container();
        this.container.bindAll(this.config.providers ?? []);

        this.expressApplication = createExpressApplication();
        this.httpServer = new HttpServer(this.expressApplication);
        this.initialized = false;
    }

    private initialize(): void {
        Resolver.resolve({
            container: this.container,
            application: this.expressApplication,
            controllers: this.config.controllers,
            middlewares: this.config.middlewares ?? []
        });
    }

    public use(middleware: ExpressHandler): void {
        this.expressApplication.use(middleware);
    }

    public get(key: string): any {
        this.expressApplication.get(key);
    }

    public set(key: string, value: unknown): void {
        this.expressApplication.set(key, value);
    }

    public listen(port: number): Promise<void> {
        if (Check.isFalse(this.initialized)) {
            this.initialize();
            this.initialized = true;
        }

        return new Promise((resolve: () => void): void => {
            this.httpServer.listen(port, (): void => resolve());
        });
    }

    public close(): Promise<void> {
        return new Promise((resolve: () => void): void => {
            this.httpServer.close((): void => resolve());
        });
    }
}
export namespace Application {
    export interface Configuration {
        readonly controllers: ReadonlyArray<Constructor>;
        readonly middlewares?: ReadonlyArray<Middleware.Constructor>;
        readonly providers?: ReadonlyArray<Constructor | Provider>;
    }
}
