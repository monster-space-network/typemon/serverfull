import { Check } from '@typemon/check';
import { Reflection, Metadata } from '@typemon/reflection';
import { Inject } from '@typemon/dependency-injection';
//
import { HandlerParameterDecorator } from './handler-parameter-decorator';
//
//
//
export class Context {
    private readonly map: Map<unknown, unknown>;

    public constructor() {
        this.map = new Map();
    }

    public has(key: unknown): boolean {
        return this.map.has(key);
    }
    public hasNot(key: unknown): boolean {
        return Check.isFalse(this.has(key));
    }

    public get(key: unknown): any {
        if (this.hasNot(key)) {
            throw new Error('Key does not exist.');
        }

        return this.map.get(key);
    }

    public set(key: unknown, value: unknown): void {
        this.map.set(key, value);
    }

    public delete(key: unknown): void {
        this.map.delete(key);
    }

    public clear(): void {
        this.map.clear();
    }
}

export type ScopedContext = Context;
export function ScopedContext(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Reflection.decorate([Inject(Context)], metadata.owner));
}
