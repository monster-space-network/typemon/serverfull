import { Reflection, Metadata } from '@typemon/reflection';
import { Inject } from '@typemon/dependency-injection';
//
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
import { HttpHeaders } from './http-headers';
//
//
//
export class HttpResponse {
    public statusCode: number;
    public readonly headers: HttpHeaders;
    public body: any;

    public constructor() {
        this.statusCode = 200;
        this.headers = new HttpHeaders();
        this.body = null;
    }
}

export type Response = HttpResponse;
export function Response(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Reflection.decorate([Inject(HttpResponse)], metadata.owner));
}
