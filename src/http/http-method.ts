//
//
//
export enum HttpMethod {
    Any = 'ANY',

    Head = 'HEAD',
    Options = 'OPTIONS',

    Get = 'GET',
    Put = 'PUT',
    Post = 'POST',
    Patch = 'PATCH',
    Delete = 'DELETE'
}
export namespace HttpMethod {
    export function from(method: string): HttpMethod {
        switch (method.toUpperCase()) {
            case HttpMethod.Any: return HttpMethod.Any;

            case HttpMethod.Head: return HttpMethod.Head;
            case HttpMethod.Options: return HttpMethod.Options;

            case HttpMethod.Get: return HttpMethod.Get;
            case HttpMethod.Put: return HttpMethod.Put;
            case HttpMethod.Post: return HttpMethod.Post;
            case HttpMethod.Patch: return HttpMethod.Patch;
            case HttpMethod.Delete: return HttpMethod.Delete;

            default: throw new Error('Invalid method.');
        }
    }
}
