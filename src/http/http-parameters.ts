import { Check } from '@typemon/check';
import { Reflection, Metadata } from '@typemon/reflection';
import { Inject, InjectionToken } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../metadata-key';
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
//
//
//
export class HttpParameters implements Iterable<[string, string]> {
    protected readonly map: Map<string, string>;

    public constructor(parameters: object = []) {
        this.map = new Map();

        const entries: Iterable<[string, string]> = Check.isNotIterable(parameters)
            ? Object.entries(parameters)
            : parameters;

        for (const [key, value] of entries) {
            this.set(key, value);
        }
    }

    public [Symbol.iterator](): Iterator<[string, string]> {
        return this.map[Symbol.iterator]();
    }

    public has(key: string): boolean {
        return this.map.has(key);
    }
    public hasNot(key: string): boolean {
        return Check.isFalse(this.has(key));
    }

    public get(key: string): string {
        const value: null | string = this.map.get(key) ?? null;

        if (Check.isNull(value)) {
            throw new Error('Parameter does not exist.');
        }

        return value;
    }

    public set(key: string, value: string): void {
        this.map.set(key, value);
    }

    public delete(key: string): void {
        this.map.delete(key);
    }

    public clear(): void {
        this.map.clear();
    }
}
export type ReadonlyHttpParameters = Omit<HttpParameters, 'set' | 'delete' | 'clear'>;

export type PathParameters = ReadonlyHttpParameters;
export function PathParameters(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Reflection.decorate([Inject(PathParameters.TOKEN)], metadata.owner));
}
export namespace PathParameters {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverfull.http.path-parameters');
}

export function PathParameter(key: string): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => {
        Reflection.decorate([Inject(PathParameters.TOKEN)], metadata.owner);
        metadata.set(MetadataKey.KEY, key);
    });
}

export type QueryStringParameters = ReadonlyHttpParameters;
export function QueryStringParameters(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Reflection.decorate([Inject(QueryStringParameters.TOKEN)], metadata.owner));
}
export namespace QueryStringParameters {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverfull.http.query-string-parameters');
}

export function QueryStringParameter(key: string): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => {
        Reflection.decorate([Inject(QueryStringParameters.TOKEN)], metadata.owner);
        metadata.set(MetadataKey.KEY, key);
    });
}
