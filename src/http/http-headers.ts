import { Check } from '@typemon/check';
import { Reflection, Metadata } from '@typemon/reflection';
import { Inject } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../metadata-key';
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
import { HttpParameters, ReadonlyHttpParameters } from './http-parameters';
//
//
//
export class HttpHeaders extends HttpParameters {
    public constructor(headers?: object) {
        super(headers);
    }

    private getKey(name: string): string {
        const normalizedName: string = name.toLowerCase();

        for (const key of this.map.keys()) {
            if (Check.equal(normalizedName, key.toLowerCase())) {
                return normalizedName;
            }
        }

        return normalizedName;
    }

    public has(name: string): boolean {
        return this.map.has(this.getKey(name));
    }
    public hasNot(name: string): boolean {
        return Check.isFalse(this.has(name));
    }

    public get(name: string): string {
        const value: null | string = this.map.get(this.getKey(name)) ?? null;

        if (Check.isNull(value)) {
            throw new Error('Header does not exist.');
        }

        return value;
    }

    public set(name: string, value: string): void {
        this.map.set(this.getKey(name), value);
    }

    public delete(name: string): void {
        this.map.delete(this.getKey(name));
    }
}
export type ReadonlyHttpHeaders = ReadonlyHttpParameters;

export type Headers = ReadonlyHttpHeaders;
export function Headers(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Reflection.decorate([Inject(HttpHeaders)], metadata.owner));
}

export function Header(name: string): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => {
        Reflection.decorate([Inject(HttpHeaders)], metadata.owner);
        metadata.set(MetadataKey.NAME, name);
    });
}
