import { Reflection, Metadata } from '@typemon/reflection';
import { Inject, InjectionToken } from '@typemon/dependency-injection';
import Express from 'express';
//
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
import { HttpMethod } from './http-method';
import { HttpHeaders, ReadonlyHttpHeaders } from './http-headers';
import { HttpParameters, ReadonlyHttpParameters } from './http-parameters';
//
//
//
export class HttpRequest {
    public readonly method: HttpMethod;
    public readonly path: string;

    public readonly headers: ReadonlyHttpHeaders;
    public readonly pathParameters: ReadonlyHttpParameters;
    public readonly queryStringParameters: ReadonlyHttpParameters;

    public constructor(
        private readonly expressRequest: Express.Request
    ) {
        this.method = HttpMethod.from(this.expressRequest.method);
        this.path = this.expressRequest.path;

        this.headers = new HttpHeaders(this.expressRequest.headers);
        this.pathParameters = new HttpParameters(this.expressRequest.params);
        this.queryStringParameters = new HttpParameters(this.expressRequest.query);
    }

    public get body(): any {
        return this.expressRequest.body;
    }
}

export type Request = HttpRequest;
export function Request(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Reflection.decorate([Inject(HttpRequest)], metadata.owner));
}

export function Body(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Reflection.decorate([Inject(Body.TOKEN)], metadata.owner));
}
export namespace Body {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverfull.http.request.body');
}
