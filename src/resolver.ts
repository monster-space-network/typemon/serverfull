import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Container, Constructor } from '@typemon/dependency-injection';
import Express from 'express';
import Boom from '@hapi/boom';
//
import { MetadataKey } from './metadata-key';
import { Controller } from './controller';
import { Handler } from './handler';
import { Middleware } from './middleware';
import { HttpMethod } from './http';
import { Runner } from './runner';
//
//
//
export namespace Resolver {
    export interface Context {
        readonly container: Container;
        readonly application: Express.Application;
        readonly controllers: ReadonlyArray<Constructor>;
        readonly middlewares: ReadonlyArray<Middleware.Constructor>;
    }

    function resolveRoute(application: Express.Application, method: HttpMethod, path: string, handler: Express.Handler): unknown {
        switch (method) {
            case HttpMethod.Any: return application.all(path, handler);

            case HttpMethod.Head: return application.head(path, handler);
            case HttpMethod.Options: return application.options(path, handler);

            case HttpMethod.Get: return application.get(path, handler);
            case HttpMethod.Put: return application.put(path, handler);
            case HttpMethod.Post: return application.post(path, handler);
            case HttpMethod.Patch: return application.patch(path, handler);
            case HttpMethod.Delete: return application.delete(path, handler);
        }
    }

    export function resolve({ container, application, controllers, middlewares }: Context): void {
        for (const target of controllers) {
            const controllerMetadata: Metadata = Metadata.of({ target });
            const controllerOptions: Controller.Options = controllerMetadata.getOwn(MetadataKey.OPTIONS);
            const propertyKeys: ReadonlyArray<string> = controllerMetadata.getOwn(MetadataKey.HANDLER_PROPERTY_KEYS);

            for (const propertyKey of propertyKeys) {
                const handlerMetadata: Metadata = Metadata.of({
                    target: target.prototype,
                    propertyKey
                });
                const handlerOptions: Handler.Options = handlerMetadata.getOwn(MetadataKey.OPTIONS);
                const method: HttpMethod = handlerOptions.method ?? HttpMethod.Any;
                const path: string = '/' + [controllerOptions.path, handlerOptions.path]
                    .filter((path: undefined | string): path is string => Check.isNotUndefined(path))
                    .join('/');

                resolveRoute(application, method, path, (request: Express.Request, response: Express.Response): Promise<void> => Runner.run({
                    container,
                    middlewares,

                    target,
                    propertyKey,

                    request,
                    response
                }));
            }
        }

        application.use((_: Express.Request, response: Express.Response): void => {
            const boom: Boom.Boom = Boom.notFound();

            response.status(boom.output.statusCode);
            response.send(boom.output.payload);
        });
    }
}
