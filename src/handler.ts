import { Check } from '@typemon/check';
import { Reflection, Metadata } from '@typemon/reflection';
import { Injectable } from '@typemon/dependency-injection';
//
import { MetadataKey } from './metadata-key';
import { Middleware } from './middleware';
import { HttpMethod } from './http';
//
//
//
export function Handler(options: Handler.Options = {}): MethodDecorator {
    return Metadata.Decorator.create((metadata: Metadata): void => {
        if (Check.isFunction(metadata.owner.target) || Check.isUndefined(metadata.owner.propertyKey) || Check.isNotUndefined(metadata.owner.parameterIndex)) {
            throw new Error('This decorator can only be used with instance methods.');
        }

        if (Check.isSymbol(metadata.owner.propertyKey)) {
            throw new Error('Invalid property key.');
        }

        const constructorMetadata: Metadata = Metadata.of({
            target: metadata.owner.target.constructor
        });
        const propertyKeys: ReadonlyArray<string> = constructorMetadata.hasOwn(MetadataKey.HANDLER_PROPERTY_KEYS)
            ? constructorMetadata.getOwn(MetadataKey.HANDLER_PROPERTY_KEYS)
            : [];

        if (propertyKeys.includes(metadata.owner.propertyKey)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        constructorMetadata.set(MetadataKey.HANDLER_PROPERTY_KEYS, propertyKeys.concat(metadata.owner.propertyKey));
        metadata.set(MetadataKey.OPTIONS, options);
        Reflection.decorate([Injectable()], metadata.owner);
    });
}
export namespace Handler {
    export interface Options {
        readonly method?: HttpMethod;
        readonly path?: string;
        readonly middlewares?: ReadonlyArray<Middleware.Constructor>;
    }
}
