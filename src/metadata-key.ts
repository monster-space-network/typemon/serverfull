//
//
//
export namespace MetadataKey {
    export const OPTIONS: unique symbol = Symbol('typemon.serverfull.options');

    export const HANDLER_PROPERTY_KEYS: unique symbol = Symbol('typemon.serverfull.handler-property-keys');

    export const NAME: unique symbol = Symbol('typemon.serverfull.name');
    export const KEY: unique symbol = Symbol('typemon.serverfull.key');
}
