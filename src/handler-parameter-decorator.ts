import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
//
//
//
export type HandlerParameterDecorator = ParameterDecorator;
export namespace HandlerParameterDecorator {
    export type Handler = Metadata.Decorator.Handler;

    export function create(handler: Handler): HandlerParameterDecorator {
        return Metadata.Decorator.create((metadata: Metadata): void => {
            if (Check.isFunction(metadata.owner.target) || Check.isUndefined(metadata.owner.parameterIndex)) {
                throw new Error('This decorator can only be used for instance method parameters.');
            }

            handler(metadata);
        });
    }
}
