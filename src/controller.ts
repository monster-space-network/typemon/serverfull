import { Reflection, Metadata } from '@typemon/reflection';
import { Injectable } from '@typemon/dependency-injection';
//
import { MetadataKey } from './metadata-key';
import { Middleware } from './middleware';
//
//
//
export function Controller(options: Controller.Options = {}): ClassDecorator {
    return Metadata.Decorator.create((metadata: Metadata): void => {
        if (metadata.hasOwn(MetadataKey.OPTIONS)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        metadata.set(MetadataKey.OPTIONS, options);

        if (metadata.hasNotOwn(MetadataKey.HANDLER_PROPERTY_KEYS)) {
            metadata.set(MetadataKey.HANDLER_PROPERTY_KEYS, []);
        }

        Reflection.decorate([Injectable()], metadata.owner);
    });
}
export namespace Controller {
    export interface Options {
        readonly path?: string;
        readonly middlewares?: ReadonlyArray<Middleware.Constructor>;
    }
}
