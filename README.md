# Serverfull - [![version](https://img.shields.io/npm/v/@typemon/serverfull.svg)](https://www.npmjs.com/package/@typemon/serverfull) [![license](https://img.shields.io/npm/l/@typemon/serverfull.svg)](https://gitlab.com/monster-space-network/typemon/serverfull/blob/master/LICENSE) ![typescript-version](https://img.shields.io/npm/dependency-version/@typemon/serverfull/dev/typescript.svg)
- Simplified dependency management with dependency injection
- Abstracted http object
- Powerful middleware, pipeline



## Installation
```
$ npm install @typemon/serverfull
```

#### [TypeScript](https://github.com/Microsoft/TypeScript)
- Version **`3.5 or later`** is required.
- Configure the decorator and metadata options.
    - https://www.typescriptlang.org/docs/handbook/decorators.html#metadata
```json
{
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true
}
```
